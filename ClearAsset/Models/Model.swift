//
//  Model.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 26/08/2021.
//

import Foundation

struct dataSource : Codable {
    let status : String?
    let code : Int?
    let message : String?
    let data : UserData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case code = "http-code"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(UserData.self, forKey: .data)
    }
}


struct UserData : Codable {
    let token : String?
    let expiration : String?
    let userinfo: Userinfo?

    enum CodingKeys: String, CodingKey {

        case token = "token"
        case expiration = "expiry"
        case userinfo = "user_information"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        expiration = try values.decodeIfPresent(String.self, forKey: .expiration)
        userinfo = try values.decodeIfPresent(Userinfo.self, forKey: .userinfo)
    }

}

struct Userinfo : Codable {
    let fullname : String?
    let avatar : String?
    let email : String?
    let mobile : String?
    let mobileFormat : String?
    let mobileVerify : Bool?
    let countryCode : String?
    let countryRegion: String?
    let phoneType : String?
    let information : Information?

    enum CodingKeys: String, CodingKey {

        case fullname = "full_name"
        case avatar = "avatar"
        case email = "email"
        case mobile = "mobile"
        case mobileFormat = "mobile_international_format"
        case mobileVerify = "is_verified_mobile"
        case countryCode = "country_code"
        case countryRegion = "country_region_code"
        case phoneType = "type_phone"
        case information = "information"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fullname = try values.decodeIfPresent(String.self, forKey: .fullname)
        avatar = try values.decodeIfPresent(String.self, forKey: .avatar)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        mobileFormat = try values.decodeIfPresent(String.self, forKey: .mobileFormat)
        mobileVerify = try values.decodeIfPresent(Bool.self, forKey: .mobileFormat)
        countryCode = try values.decodeIfPresent(String.self, forKey: .countryCode)
        countryRegion = try values.decodeIfPresent(String.self, forKey: .countryRegion)
        phoneType = try values.decodeIfPresent(String.self, forKey: .phoneType)
        information = try values.decodeIfPresent(Information.self, forKey: .information)
    }
}

struct Information : Codable {
    let zip : String?
    let city : String?
    let state : String?
    let country : String?
    let primary : String?
    let birthday : String?
    let currency : String?
    let interested : String?
    let addressOne : String?
    let addressTwo : String?

    enum CodingKeys: String, CodingKey {

        case zip = "zip"
        case city = "city"
        case state = "state"
        case country = "country"
        case primary = "primary"
        case birthday = "birthday"
        case currency = "currency"
        case interested = "interested"
        case addressOne = "address_one"
        case addressTwo = "address_two"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        zip = try values.decodeIfPresent(String.self, forKey: .zip)
        city = try values.decodeIfPresent(String.self, forKey: .city)
        state = try values.decodeIfPresent(String.self, forKey: .state)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        primary = try values.decodeIfPresent(String.self, forKey: .primary)
        birthday = try values.decodeIfPresent(String.self, forKey: .birthday)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        interested = try values.decodeIfPresent(String.self, forKey: .interested)
        addressOne = try values.decodeIfPresent(String.self, forKey: .addressOne)
        addressTwo = try values.decodeIfPresent(String.self, forKey: .addressTwo)
    }

}
