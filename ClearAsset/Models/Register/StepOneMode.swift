//
//  StepOneMode.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 27/08/2021.
//

import Foundation

// server response
struct RegisterStep1BaseModel : Codable {
    let data : RegisterStep1Model?

    enum CodingKeys: String, CodingKey {

        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(RegisterStep1Model.self, forKey: .data)
    }

}


struct RegisterStep1Model : Codable {
    let status : Bool?
    let info : Step1InfoModel?
    let errors: DataStep1Errors?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case info = "get"
        case errors = "errors"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        info = try values.decodeIfPresent(Step1InfoModel.self, forKey: .info)
        errors = try values.decodeIfPresent(DataStep1Errors.self, forKey: .errors)
    }

}


struct Step1InfoModel : Codable {
    let email : String?
    let password : String?
    let password_confirmation : String?

    enum CodingKeys: String, CodingKey {

        case email = "email"
        case password = "password"
        case password_confirmation = "password_confirmation"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        password_confirmation = try values.decodeIfPresent(String.self, forKey: .password_confirmation)
    }

}

//Error model

struct DataStep1Errors : Codable {
    let errors : Step1ErrorsModel?

    enum CodingKeys: String, CodingKey {

        case errors = "errors"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        errors = try values.decodeIfPresent(Step1ErrorsModel.self, forKey: .errors)
    }

}


struct Step1ErrorsModel : Codable {
    let email : [String]?
    let password: [String]?
    let rePassword: [String]?

    enum CodingKeys: String, CodingKey {

        case email = "email"
        case password = "password"
        case rePassword = "password_confirmation"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        email = try values.decodeIfPresent([String].self, forKey: .email)
        password = try values.decodeIfPresent([String].self, forKey: .password)
        rePassword = try values.decodeIfPresent([String].self, forKey: .rePassword)
    }

}
