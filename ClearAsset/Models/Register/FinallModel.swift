//
//  FinalMode.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 27/08/2021.
//

import Foundation



struct RegisterModel : Codable {
    let data : DataUsers?

    enum CodingKeys: String, CodingKey {

        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(DataUsers.self, forKey: .data)
    }

}


struct DataUsers : Codable {
    let errors : ErrorsModel?

    enum CodingKeys: String, CodingKey {

        case errors = "errors"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        errors = try values.decodeIfPresent(ErrorsModel.self, forKey: .errors)
    }

}



struct ErrorsModel : Codable {
    let email : [String]?
    let mobile : [String]?

    enum CodingKeys: String, CodingKey {

        case email = "email"
        case mobile = "mobile"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        email = try values.decodeIfPresent([String].self, forKey: .email)
        mobile = try values.decodeIfPresent([String].self, forKey: .mobile)
    }

}
