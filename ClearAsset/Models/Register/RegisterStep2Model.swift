//
//  DevelopeModel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 27/08/2021.
//

import Foundation



struct RegisterStep2BaseModel : Codable {
    let data : RegisterStep2Model?

    enum CodingKeys: String, CodingKey {

        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(RegisterStep2Model.self, forKey: .data)
    }

}


struct RegisterStep2Model : Codable {
    let status : Bool?
    let get : Step2InfoModel?
    let errors: DataStep2Errors?
    
    enum CodingKeys: String, CodingKey {

        case status = "status"
        case get = "get"
        case errors = "errors"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
        get = try values.decodeIfPresent(Step2InfoModel.self, forKey: .get)
        errors = try values.decodeIfPresent(DataStep2Errors.self, forKey: .errors)
    }

}


struct Step2InfoModel : Codable {
    let type : String?
    let currency : String?
    let full_name : String?
    let birthday : String?

    enum CodingKeys: String, CodingKey {

        case type = "type"
        case currency = "currency"
        case full_name = "full_name"
        case birthday = "birthday"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        full_name = try values.decodeIfPresent(String.self, forKey: .full_name)
        birthday = try values.decodeIfPresent(String.self, forKey: .birthday)
    }

}

// Errrors 
struct DataStep2Errors : Codable {
    let errors : Step2ErrorsModel?

    enum CodingKeys: String, CodingKey {

        case errors = "errors"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        errors = try values.decodeIfPresent(Step2ErrorsModel.self, forKey: .errors)
    }

}

struct Step2ErrorsModel: Codable {
    let type: String?
    let currency: String?
    let fullname: String?
    let birthday: String?
    
    enum CodingKeys: String, CodingKey {
        
        case type = "type"
        case currency = "currency"
        case fullname = "full_name"
        case birthday = "birthday"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        currency = try values.decodeIfPresent(String.self, forKey: .currency)
        fullname = try values.decodeIfPresent(String.self, forKey: .fullname)
        birthday = try values.decodeIfPresent(String.self, forKey: .birthday)
    }
}
