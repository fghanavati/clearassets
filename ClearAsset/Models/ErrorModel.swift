//
//  ErrorModel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 26/08/2021.
//

import Foundation


struct ErrorModel {
    let title: String?
    let message: String?
}
