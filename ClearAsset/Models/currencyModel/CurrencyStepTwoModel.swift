//
//  CurrencyStepTwoModel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 28/08/2021.
//


import Foundation


struct CurrencyDevelopedModel : Codable {
    let status : String?
    let coder : Int?
    let message : String?
    let data : DataInfo?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case coder = "http-code"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        coder = try values.decodeIfPresent(Int.self, forKey: .coder)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(DataInfo.self, forKey: .data)
    }

}



struct DataInfo: Codable {
    let country : [Country]?

    enum CodingKeys: String, CodingKey {

        case country = "country"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        country = try values.decodeIfPresent([Country].self, forKey: .country)
    }

}



struct Country : Codable {
    let name : String?
    let code : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case code = "code"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        code = try values.decodeIfPresent(String.self, forKey: .code)
    }

}
