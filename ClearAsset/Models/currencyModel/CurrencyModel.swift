//
//  CurrencyModel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 27/08/2021.
//

import Foundation


struct CurrencyModel : Codable {
    let data : DataCurrency?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(DataCurrency.self, forKey: .data)
    }

}


struct DataCurrency : Codable {
    let currency : [Currencyinfo]?

    enum CodingKeys: String, CodingKey {
        case currency = "currency"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        currency = try values.decodeIfPresent([Currencyinfo].self, forKey: .currency)
    }

}


struct Currencyinfo : Codable {
    let name : String?
    let value : String?

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case value = "value"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        value = try values.decodeIfPresent(String.self, forKey: .value)
    }

}
