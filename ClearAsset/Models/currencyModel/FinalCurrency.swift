//
//  FinalCurrency.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 28/08/2021.
//

import Foundation


struct FinalCurrencyModel : Codable {
    let status : String?
    let codes : Int?
    let message : String?
    let data : CurrencyData?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case codes = "http-code"
        case message = "message"
        case data = "data"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        codes = try values.decodeIfPresent(Int.self, forKey: .codes)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        data = try values.decodeIfPresent(CurrencyData.self, forKey: .data)
    }

}

struct CurrencyData : Codable {
    let primary : [String]?

    enum CodingKeys: String, CodingKey {

        case primary = "primary"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        primary = try values.decodeIfPresent([String].self, forKey: .primary)
    }

}
