//
//  CurrencyViewModel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 31/08/2021.
//



import Foundation

typealias currencyCompletion = (CurrencyModel?, ErrorModel?) -> Void
protocol CurrencyProtocol {
    func currency(name: String?, value: String?, completion: @escaping currencyCompletion)
}

class CurrencyViewModel {
    let decoder = JSONDecoder()
}


extension CurrencyViewModel: CurrencyProtocol {
    func currency(name: String?, value: String?, completion: @escaping currencyCompletion) {
        
        guard let name = name else {
            completion(nil, ErrorModel(title: "Error", message: "Select the currency"))
            return
        }
        guard let value = value else {
            completion(nil, ErrorModel(title: "Error", message: "Select the vlaue"))
            return
        }
        
        let webServices = NetworkingService()
        webServices.getRequest(url: URL(string: "http://clear-asset.com/api/v1/user/get/currency")!) { response, error in
            if error == nil {
                do {
                    if let data = response {
                        let decoded = try self.decoder.decode(CurrencyModel.self, from: data)
                        completion(decoded, nil)
                        
                    }
                    
                } catch {
                    completion(nil, ErrorModel(title: "Error", message: "faild to Encode Json"))
                }
                
            } else {
                completion(nil, ErrorModel(title: "Error", message: error!.localizedDescription))
            }
        }
    }
}
