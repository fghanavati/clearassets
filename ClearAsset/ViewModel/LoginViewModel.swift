//
//  LoginViewModel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 26/08/2021.
//

import Foundation


typealias loginCompletion = (UserData?, ErrorModel?) -> Void

protocol LoginProtocol {
    
    func login(email: String?, password: String?, completion: @escaping loginCompletion)
}


class LoginViewModel {
    
    let decoder = JSONDecoder()
}


extension LoginViewModel: LoginProtocol {
    func login(email: String?, password: String?, completion: @escaping loginCompletion) {
        
        guard let email = email ,  email.count >= 8 else {
            completion(nil, ErrorModel(title: "Error", message: "Email is not valid"))
            return
        }
        guard let password = password , password.count >= 8  else {
            completion(nil, ErrorModel(title: "Error", message: "Password is not valid"))
            return
        }
        
        
        let webServices = NetworkingService()
        webServices.postRequest(url: URL(string: "http://clear-asset.com/api/v1/user/login")!, parameters: ["email": email , "password": password], headers: nil) { response, error in
            
              if error == nil {
                do {
                    
                    if let data = response {
                        let decoded = try self.decoder.decode(UserData.self, from: data)
                        completion(decoded, nil)
                    }
                    
                } catch {
                    completion(nil, ErrorModel(title: "Error", message: "Username is not valid"))
                }
              } else {
                completion(nil, ErrorModel(title: "Error", message: error!.localizedDescription))
            }
        }
    }
}


 
