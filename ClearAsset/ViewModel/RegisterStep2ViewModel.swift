//
//  AddRegisterModel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 28/08/2021.
//

import Foundation


typealias step2Completion = (RegisterStep2BaseModel?, ErrorModel?) -> Void

protocol RegisterStep2Protocol {
    func registerStep2(type: String?, currency: String?, fullname: String?, birthday: String?, completion: @escaping step2Completion)
    
}


class RegisterStep2ViewModel {
    let decoder = JSONDecoder()
    func isValid(testStr:String) -> Bool {
        guard testStr.count > 7, testStr.count < 18 else { return false }

        let predicateTest = NSPredicate(format: "SELF MATCHES %@", "^(([^ ]?)(^[a-zA-Z].*[a-zA-Z]$)([^ ]?))$")
        return predicateTest.evaluate(with: testStr)
    }
}


extension RegisterStep2ViewModel: RegisterStep2Protocol {
    func registerStep2(type: String?, currency: String?, fullname: String?, birthday: String?, completion: @escaping step2Completion) {
        
        guard let type = type, type == "personal", type == "business" else {
            completion(nil, ErrorModel(title: "Error", message: "The type field is required"))
            return
        }
        guard let currency = currency, currency == "usd" else {
            completion(nil, ErrorModel(title: "Error", message: "The currency field is required"))
            return
        }
        guard let fullname = fullname, isValid(testStr: fullname)  else {
            completion(nil, ErrorModel(title: "Error", message: "The fullname field is required"))
            return
        }
        guard let birthday = birthday , birthday == "MM/dd/yyyy" else {
            completion(nil, ErrorModel(title: "Error", message: "The birthday field is required"))
            return
        }
        
        let webServices = NetworkingService()
        webServices.postRequest(url: URL(string: "http://clear-asset.com/api/v1/user/register/validate/step/2")!, parameters: ["type": type, "currency": currency, "full_name": fullname, "birthday": birthday], headers: nil) { response, error in
            
            if error == nil {
               //success
                do {
                    if let data = response {
                        let decoded = try self.decoder.decode(RegisterStep2BaseModel.self, from: data)
                        completion(decoded, nil)
                    }
                } catch {
                  completion(nil, ErrorModel(title: "Error", message: "failed to decode JSON"))
                }
            } else {
              // failure
                do {
                    
                    if let data = response {
                        let decoded = try self.decoder.decode(RegisterStep2BaseModel.self, from: data)
                        if let receivedData = decoded.data, let dataErrors = receivedData.errors, let errors = dataErrors.errors {
                            
                            if let typeError = errors.type {
                                completion(nil, ErrorModel(title: "Error", message: typeError))
                            }
                            if let currencyError = errors.currency {
                                completion(nil, ErrorModel(title: "Error", message: currencyError))
                            }
                            if let fullnameError = errors.fullname {
                                completion(nil, ErrorModel(title: "Error", message: fullnameError))
                            }
                            if let birthdayError = errors.birthday {
                                completion(nil, ErrorModel(title: "Error", message: birthdayError))
                            }
                        }
                    }
                } catch {
                    completion(nil, ErrorModel(title: "Error", message: "failed to decode JSON"))
                }
            }
        }
    }
}
