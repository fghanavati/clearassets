//
//  RegisterViewModel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 28/08/2021.
//

import Foundation


typealias RegisterStep1Completion = (RegisterStep1BaseModel?, ErrorModel?) -> Void

protocol RegisterStep1Protocol {
    func registerStep1(email: String?, password: String?, confirmPassword: String?, completion: @escaping RegisterStep1Completion)
}

class RegisterStep1ViewModel {
    let decoder = JSONDecoder()

    fileprivate func isEmailValid(_ email: String?) -> Bool {
        guard let userEmail = email, !userEmail.isEmpty else {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: userEmail)
    }
}


extension RegisterStep1ViewModel: RegisterStep1Protocol {
    func registerStep1(email: String?, password: String?, confirmPassword: String?, completion: @escaping RegisterStep1Completion) {

        guard let email = email, isEmailValid(email) else {
            completion(nil, ErrorModel(title: "Error", message: "Email is not valid"))
            return
        }
        guard let password = password, password.count >= 6 else {
            completion(nil, ErrorModel(title: "Error", message: "Password is not valid"))
            return
        }
        guard let confirmPassword = confirmPassword else {
            completion(nil, ErrorModel(title: "Error", message: "Confirm password isn't valid"))
            return
        }
        
        if confirmPassword != password {
            completion(nil, ErrorModel(title: "Error", message: "Confirm password doesn't match"))
            return
        }
        
        let webService = NetworkingService()
        webService.postRequest(url: URL(string: "http://clear-asset.com/api/v1/user/register/validate/step/1")!, parameters: ["email": email, "password": password, "password_confirmation": confirmPassword], headers: nil) { response, error in

            if error == nil {
                //seccuss response
                // try - throw
                do {
                    if let data = response {
                        let decoded = try self.decoder.decode(RegisterStep1BaseModel.self, from: data)
                        completion(decoded, nil)
                    }
                } catch {
                    completion(nil, ErrorModel(title: "Error", message: "failed to decode JSON"))
                }

            } else {
                //errror response
                do {
                    if let data = response {
                        let decoded = try self.decoder.decode(RegisterStep1BaseModel.self, from: data)
                        if let receivedData = decoded.data, let dataErrors = receivedData.errors, let errors = dataErrors.errors {
                            if let errorEmail = errors.email {
                                completion(nil, ErrorModel(title: "Error", message: errorEmail[0]))
                                return
                            } else if let passwordError = errors.password {
                                completion(nil, ErrorModel(title: "Error", message: passwordError[0]))
                                return
                            } else if let repasswordError = errors.rePassword {
                                completion(nil, ErrorModel(title: "Error", message: repasswordError[0]))
                                return
                            }
                        }
                    }
                } catch {
                    completion(nil, ErrorModel(title: "Error", message: "failed to decode JSON"))
                }
            }
        }
    }
}
