//
//  StarterViewController.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 22/08/2021.
//

import UIKit

class StarterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func didTapStartButton(_ sender: Any) {
        
        let sb = UIStoryboard(name: "Login", bundle: nil)
        if let loginVC = sb.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            navigationController?.pushViewController(loginVC, animated: true)
        }
    }
}
