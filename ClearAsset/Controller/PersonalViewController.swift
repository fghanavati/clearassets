//
//  PersonalViewController.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 25/08/2021.
//

import UIKit
import DropDown

class PersonalViewController: UIViewController {

    
    @IBOutlet weak var personalView: UIView!
    @IBOutlet weak var currencyView: UIView!
    @IBOutlet weak var fullNameTextFiled: UITextField!
    @IBOutlet weak var birthDateView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var personalImage: UIImageView!
    @IBOutlet weak var currencyImage: UIImageView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var personalLeable: UILabel!
  
    //MARK: - var
    var dropDownCurrency: DropDown!
    var dropDownType: DropDown!
    var registerStep2ViewModel : RegisterStep2ViewModel!
    var currencyViewModel : CurrencyViewModel!
    
    fileprivate func initialize() {
        registerStep2ViewModel = RegisterStep2ViewModel()
        currencyViewModel = CurrencyViewModel()
    }

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        personalView.layer.cornerRadius = 10
        currencyView.layer.cornerRadius = 10
        personalView.layer.borderWidth = 1
        currencyView.layer.borderWidth = 1
        personalView.layer.borderColor = UIColor(named: "appBorder")?.cgColor
        currencyView.layer.borderColor = UIColor(named: "appBorder")?.cgColor
        initBackView()
        initialize()
        setupDropDown()
        tapGuesture()
        currency()
        addGuesterToCurrencyView()
        setupCurrencyDropDown()
        initViews()
        
        // Do any additional setup after loading the view.
    }

    fileprivate func initViews() {
        birthDateView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOnBirthdate(_:))))
    }

    @objc func didTapOnBirthdate(_ sender: UITapGestureRecognizer) {
        let datePicker = UIDatePicker(frame: CGRect(x: 20, y: 200, width: self.view.frame.width - 50, height: 400))
        self.view.addSubview(datePicker)
        datePicker.datePickerMode = .date
        print("hello")
    }
    
    fileprivate func setupDropDown() {
        dropDownType = DropDown()
        dropDownType.anchorView = personalView
        dropDownType.dataSource = ["Business", "Personal"]
       // dropDown.direction = .bottom
        dropDownType.bottomOffset = CGPoint(x: 0, y:(dropDownType.anchorView?.plainView.bounds.height)!)
        dropDownType.selectionAction = { [unowned self] (index: Int, item: String) in
        personalLeable.text = item
        }
    }


    func tapGuesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapGuesture(_:)))
        personalView.addGestureRecognizer(tap)
    }
    
    @objc func didTapGuesture (_ sender: UITapGestureRecognizer) {
        dropDownType.show()
    }
    
    fileprivate func setupCurrencyDropDown() {
        dropDownCurrency = DropDown()
        dropDownCurrency.anchorView = currencyView
        
       // dropDown.direction = .bottom
        dropDownCurrency.bottomOffset = CGPoint(x: 0, y:(dropDownCurrency.anchorView?.plainView.bounds.height)!)
        dropDownCurrency.selectionAction = { [unowned self] (index: Int, item: String) in
        currencyLabel.text = item
        }
    }
    
    
    fileprivate func initizieCurrencyDropDownDataSource(currencyList: [Currencyinfo]) {
        currencyList.forEach { currency in
            if let name = currency.name {
                dropDownCurrency.dataSource.append(name)
            }
        }
    }
    
    fileprivate func addGuesterToCurrencyView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapOnCurrencyView(_:)))
        currencyView.addGestureRecognizer(tap)
    }
    
    @objc func didTapOnCurrencyView(_ sender: UITapGestureRecognizer) {
        dropDownCurrency.show()
    }

    
    // MARK: - APi
    fileprivate func registerStep2() {
        registerStep2ViewModel.registerStep2(type: personalLeable.text, currency: currencyLabel.text, fullname: fullNameTextFiled.text, birthday: nil) { response, error in
            if error == nil {
                // success
                self.goTOMailingInformation()
            } else {
                // failure
                self.showErrorAlert(error: error!)
                
            }
        }
    }
    
    fileprivate func currency() {
        currencyViewModel.currency(name: personalLeable.text, value: currencyLabel.text) { response, error in
            if error == nil {
              
                if let response = response, let currencyData = response.data, let currencies = currencyData.currency {
                    self.initizieCurrencyDropDownDataSource(currencyList: currencies)
                }
            } else {
                
                self.showErrorAlert(error: error!)
            }
        }
    }

    
    fileprivate func goTOMailingInformation() {
        let sb = UIStoryboard(name: "Register", bundle: nil)
        if let Vc = sb.instantiateViewController(withIdentifier: "MailInfoViewController") as? MailInfoViewController {
            navigationController?.pushViewController(Vc, animated: true)
        }
    }
    
    fileprivate func showErrorAlert(error: ErrorModel) {
        let alert = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    

    @IBAction func personalPressedButton(_ sender: Any) {
        registerStep2()
    }
    
    fileprivate func initBackView() {
        backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapBackButton(_:))))
    }
    
    @objc fileprivate func didTapBackButton(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
}
