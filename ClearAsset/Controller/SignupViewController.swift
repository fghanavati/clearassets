//
//  SignupViewController.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 25/08/2021.
//

import UIKit

class SignupViewController: BaseViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var rePasswordTextField: UITextField!
    @IBOutlet weak var ViewPassword: UIView!
    @IBOutlet weak var viewRepassword: UIView!
    @IBOutlet weak var passordEye: UIImageView!
    @IBOutlet weak var rePassEyes: UIImageView!
    
    //MARK: - var Init
    
    var registerViewModel: RegisterStep1ViewModel!
    
    fileprivate func initilize() {
        registerViewModel = RegisterStep1ViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPasswordAndRePasswordView()
        initView()
        initVieweye()
        initilize()

    }

//MARK: - other method

    fileprivate func setupPasswordAndRePasswordView() {
        viewRepassword.layer.cornerRadius = 10
        ViewPassword.layer.cornerRadius = 10
        viewRepassword.layer.borderWidth = 1
        ViewPassword.layer.borderWidth = 1
        viewRepassword.layer.borderColor = UIColor(named: "appBorder")?.cgColor
        ViewPassword.layer.borderColor = UIColor(named: "appBorder")?.cgColor
        passwordTextField.isSecureTextEntry = true
        rePasswordTextField.isSecureTextEntry = true
    }
    
    //MARK: -Api
    fileprivate func registerStep1() {
        showLoading()
        registerViewModel.registerStep1(email: emailTextField.text, password: passwordTextField.text, confirmPassword: rePasswordTextField.text) { response, error in

            self.hideLoading()
            
            if error == nil {
                // success
                self.goToPersonalPage()
            } else {
                // error
                self.registerAlert(error: error!)
            }
        }
    }
    
    
    fileprivate func goToPersonalPage() {
        let sb = UIStoryboard(name: "Register", bundle: nil)
        if let VC = sb.instantiateViewController(withIdentifier: "PersonalViewController") as? PersonalViewController {
            navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    fileprivate func registerAlert(error: ErrorModel) {
        let alert = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .cancel)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Action

    @IBAction func singupButton(_ sender: Any) {

        registerStep1()
//        let sb = UIStoryboard(name: "Register", bundle: nil)
//        if let VC = sb.instantiateViewController(withIdentifier: "PersonalViewController") as? PersonalViewController {
//            navigationController?.pushViewController(VC, animated: true)
//        }

    }
    
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    fileprivate func initView() {
        passordEye.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTappassword(_:))))
    }

    @objc fileprivate func didTappassword(_ sender: UITapGestureRecognizer) {
        //passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
        print("hello")
    }
    
    
    fileprivate func initVieweye() {
        rePassEyes.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapRePassword(_:))))
    }
    
    @objc fileprivate func didTapRePassword(_ sender: UITapGestureRecognizer) {
        print("hello")
    }
}
