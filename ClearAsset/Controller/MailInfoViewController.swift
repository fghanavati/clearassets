//
//  MailInfoViewController.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 26/08/2021.
//

import UIKit

class MailInfoViewController: UIViewController {

  
    
    @IBOutlet weak var countryTextField: CustomTextField!
    @IBOutlet weak var phonenumberTextField: CustomTextField!
    @IBOutlet weak var addressOneTextField: CustomTextField!
    @IBOutlet weak var optionalAddressTextField: CustomTextField!
    @IBOutlet weak var cityTextField: CustomTextField!
    @IBOutlet weak var stateTextField: CustomTextField!
    @IBOutlet weak var zipTextField: CustomTextField!
    @IBOutlet weak var buyingTextField: CustomTextField!
    @IBOutlet weak var primaryTextField: CustomTextField!
    @IBOutlet weak var backViewScroll: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func mailingButton(_ sender: Any) {
     let sb = UIStoryboard(name: "Register", bundle: nil)
        if let VC = sb.instantiateViewController(withIdentifier: "SkipViewController") as? SkipViewController {
            navigationController?.pushViewController(VC, animated: true)
        }
    }
    
    
    fileprivate func initView() {
        backViewScroll.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapGuester(_:))))
    }
    
    @objc fileprivate func didTapGuester(_ sender: UITapGestureRecognizer) {
        navigationController?.popViewController(animated: true)
    }
}
