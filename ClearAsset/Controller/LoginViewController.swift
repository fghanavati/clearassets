//
//  LoginViewController.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 23/08/2021.
//

import UIKit

class LoginViewController: UIViewController {

  
    @IBOutlet weak var loginViewlable: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: - Var init
    var loginViewModel: LoginViewModel!
    
    fileprivate func initView() {
        loginViewModel = LoginViewModel()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
    }
    
    
    //MARK: - Api
    
    fileprivate func loginUser() {
        loginViewModel.login(email: emailTextField.text, password: passwordTextField.text) { response, error in
            
            if error == nil {
                // success
                self.goToHomePage()
            } else {
                // Error
                self.showErrorAlert(error: error!)
            }
        }
    }
    
    fileprivate func goToHomePage() {
        let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
        if let homeViewController = homeStoryboard.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
            navigationController?.pushViewController(homeViewController, animated: true)
        }
    }
    
    fileprivate func showErrorAlert(error: ErrorModel) {
        let alert = UIAlertController(title: error.title, message: error.message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Ok", style: .cancel)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: - Action
    // didTapOnForgetButton
    @IBAction func forgotButton(_ sender: Any) {
    
    }
    
    // didTapLoginButton
    @IBAction func LoginButtonKey(_ sender: Any) {
        loginUser()
    }
    
    @IBAction func singupButtonKey(_ sender: Any) {
        let sb = UIStoryboard(name: "Register", bundle: nil)
        if let VC = sb.instantiateViewController(withIdentifier: "SignupViewController") as? SignupViewController {
            navigationController?.pushViewController(VC, animated: true)
        }
    }
    
}
