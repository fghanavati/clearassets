//
//  DropDownCell.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 01/09/2021.
//

import UIKit

class DropDownCell: UITableViewCell {

  
    @IBOutlet weak var dropView: UIView!
    @IBOutlet weak var dropDownimage: UIImageView!
    @IBOutlet weak var dropCellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
}
