//
//  CustomLabel.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 25/08/2021.
//

import Foundation
import UIKit



class CustomLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCustomLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupCustomLabel()
    }
    
    
    func setupCustomLabel() {
        
        textColor = UIColor(named: "appAddTitle")
        font = UIFont(name: "Nunito-Regular", size: 14)
        
    }
}

