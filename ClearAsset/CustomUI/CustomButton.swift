//
//  CustomButton.swift
//  ClearAsset
//
//  Created by Fatemeh Ghanavati  on 22/08/2021.
//

import UIKit



class CustomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupButton()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupButton()
    }
    
    func setupButton() {
        
        backgroundColor = UIColor(named: "appOrange")
        setTitleColor(.white, for: .normal)
        setTitleColor(.white, for: .selected)
        self.layer.cornerRadius = 10
        titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        // add custom font: nunito
    }
}
