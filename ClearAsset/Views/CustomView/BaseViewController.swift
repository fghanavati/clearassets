//
//  BaseViewController.swift
//  ClearAsset
//
//  Created by MohammadReza on 9/3/21.
//

import Foundation
import UIKit
import NVActivityIndicatorView

class BaseViewController: UIViewController {

    var loading: NVActivityIndicatorView!
    var darkView: UIView!

    override func viewDidLoad() {

        setupLoadingView()
        setupDarkView()
    }

    fileprivate func setupLoadingView() {
        loading = NVActivityIndicatorView(frame: CGRect(x: view.frame.width/2 - 50, y: view.frame.height/2 - 50, width: 100, height: 100), type: .ballScaleRippleMultiple, color: .black, padding: 10)
    }

    public func showLoading() {
        self.view.addSubview(loading)
        UIView.animate(withDuration: 0.6) {
            self.darkView.isHidden = false
        }
        loading.startAnimating()
    }

    public func hideLoading() {
        loading.stopAnimating()
        UIView.animate(withDuration: 0.6) {
            self.darkView.isHidden = true
        }
    }

    fileprivate func setupDarkView() {
        darkView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        self.view.addSubview(darkView)
        darkView.isHidden = true
        darkView.backgroundColor = .black
        darkView.alpha = 0.3
    }
}
